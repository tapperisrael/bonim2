'use strict';
angular.module('main')
    .controller('DefectCtrl', function ($localStorage, Restangular, FileService, $state, $ionicPopup, $scope, defect, apartment) {

        $scope.defect = defect;
        $scope.apartment = apartment;
        $scope.newComment = {};

        $scope.changeStatus = changeStatus;
        $scope.deleteTicket = deleteTicket;
        $scope.addComment = addComment;
        $scope.deleteImage = deleteImage;

        function changeStatus (status) {

            $scope.defect.closed = status;
            $scope.defect.patch().then(function(){

                var title = status == 1 ? "Closed!" : "Opened!";

                $ionicPopup.alert({title: title}).then(function(){

                    $state.go('main.buildings.building.apartments.apartment.defects', {building: apartment.building_id, apartment : apartment.id}, {reload: true});

                });

            });

        }

        function deleteTicket() {

            $scope.defect.remove().then(function(){

                $state.go('main.buildings.building.apartments.apartment.defects', {building: apartment.building_id, apartment : apartment.id}, {reload: true});

            })

        }

        function deleteImage() {

            delete $scope.newComment.file;

        }

        function addComment() {

            var formData = new FormData();
            formData.append('defect_id', $scope.defect.id);
            formData.append('user_id', $localStorage.id);
            formData.append('content', $scope.newComment.content);

            if ($scope.newComment.file){

                FileService.convertFileUriToFile($scope.newComment.file).then(function(file){

                    formData.append('file', file);

                    Restangular.one('defects', $scope.defect.id).all('comments').withHttpConfig({transformRequest: angular.identity}).upload(formData).then(function (data) {$state.reload();})

                })


            } else
                Restangular.one('defects', $scope.defect.id).all('comments').withHttpConfig({transformRequest: angular.identity}).upload(formData).then(function (data) {$state.reload();})

        }

    });
