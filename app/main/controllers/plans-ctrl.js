'use strict';
angular.module('main')
    .controller('PlansCtrl', function ($log, $scope, apartment) {

        $scope.apartment = apartment;

        $scope.openPlan = openPlan;
        $scope.sharePlan = sharePlan;

        function openPlan(x) {cordova.InAppBrowser.open(x, '_blank', 'location=yes');}

        function sharePlan(url) {

            var options = {subject: 'תוכניות', url: url};

            var onSuccess = function(result) {console.log("success");};

            var onError = function(msg) {console.log("fail");};

            window.plugins.socialsharing.shareWithOptions(options, onSuccess, onError);

        }

    });
