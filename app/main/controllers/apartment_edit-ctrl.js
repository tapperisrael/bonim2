'use strict';
angular.module('main')
.controller('ApartmentEditCtrl', function ($ionicScrollDelegate, Utils, $cordovaCamera, $ionicPopover, FileService, $q, $ionicModal, Restangular, $ionicPopup, $scope, apartment, WizardHandler, product_categories, plan_categories, building) {

    $scope.apartment = apartment;
    $scope.product_categories = product_categories;
    $scope.plan_categories = plan_categories;
    $scope.building = building;
    $scope.selection = 1;
    $scope.newProducts = [];
    $scope.product_category = null;
    $scope.plan_category = null;

    $scope.changeSelection = changeSelection;
    $scope.getStep = function getStep (){return WizardHandler.wizard().currentStepNumber();};
    $scope.selectProduct = selectProduct;
    $scope.deselectProduct = deselectProduct;
    $scope.isProductSelected = isProductSelected;
    $scope.saveStep1 = saveStep1;
    $scope.saveStep2 = saveStep2;
    $scope.showDeleteProductPopup = showDeleteProductPopup;
    $scope.showProductsModal = showProductsModal;
    $scope.closeProductsModal = closeProductsModal;
    $scope.planExists = planExists;
    $scope.openPlanPopover = openPlanPopover;
    $scope.deletePlan = deletePlan;
    $scope.getTitle = getTitle;
    $scope.uploadFile = uploadFile;

    $ionicPopover.fromTemplateUrl('main/templates/popover_plan.html', {scope: $scope}).then(function(popover) {$scope.popover = popover;});

    function changeSelection(x){

        $scope.selection = x;
        $ionicScrollDelegate.scrollTop();

    }

    function saveStep1() {

        $scope.apartment.save().then(function(){WizardHandler.wizard().next()});
        // WizardHandler.wizard().next();

    }

    function saveStep2() {

        var promises = [];

        angular.forEach($scope.newProducts, function (newProduct) {

            var deferred = $q.defer();
            promises.push(deferred.promise);

            if (newProduct.file) {

                FileService.convertFileUriToFile(newProduct.file).then(function (file) {

                    var formData = new FormData();

                    formData.append("file", file);
                    formData.append("title", newProduct.title);
                    formData.append("provider", newProduct.provider);
                    formData.append("refund", String(newProduct.refund));
                    formData.append("description", newProduct.description);
                    formData.append("product_category_id", String(newProduct.product_category_id));

                    Restangular.all('products').withHttpConfig({transformRequest: angular.identity}).upload(formData)

                        .then(function (data) {

                            Restangular.one('apartments', $scope.apartment.id).one('products', data.id).all('relationship').post()

                                .then(function(){deferred.resolve();});

                        }, function (error) {

                            console.log(error);

                        });

                });
            }

        });

        $q.all(promises).then(function(){

            WizardHandler.wizard().next();

        });
    }

    function getTitle() {

        var title = '';

        switch(WizardHandler.wizard().currentStepNumber()){

            case 1:
                title = 'שלב 2 - עריכת דירה';
                break;

            case 2:
                title = 'שלב 3 - מפרט';
                break;

            case 3:
                title = 'שלב 4 - תוכניות';
                break;

            case 4:
                title = 'שלב 5 - סיום';
                break;

            default:
                title = 'הקמת דירה' ;

        }

        return title;

    }


    function showDeleteProductPopup(product) {

            $ionicPopup.confirm({
                title: 'Product deletion',
                template: 'Are you sure that you want to delete this product from all apartments?'
            }).then(function(res) {

                if(res) {

                    Restangular.one('products', product.id).all('breakAll').post().then(function(){

                        $ionicPopup.alert({title: "Successfully deleted!"});

                        Restangular.one('apartments', $scope.apartment.id).get().then(function(data){$scope.apartment = data;})

                    });

                }
            });


    }

    function selectProduct (product) {

        Restangular.one('apartments', $scope.apartment.id).one('products', product.id).all('relationship').post()

            .then(function(){

                Restangular.one('apartments', $scope.apartment.id).get()

                    .then(function(data){$scope.apartment = data;})

            });

    }

    function deselectProduct (productid) {

        Restangular.one('apartments', $scope.apartment.id).one('products', productid).all('break').post().then(function(){

            Restangular.one('apartments', $scope.apartment.id).get()

                .then(function(data){$scope.apartment = data;})

        });

    }

    function isProductSelected (x) {

        for (var product = 0; product < $scope.apartment.products.length; product++){

            if ($scope.apartment.products[product].id == x)
                return true;

        }

    }

    function planExists (x) {

        for (var plan = 0; plan < $scope.apartment.plans.length; plan++){

            if ($scope.apartment.plans[plan].plan_category_id == x)
                return true;

        }

    }

    function showProductsModal(product_category_id) {

        $ionicModal.fromTemplateUrl('main/templates/modal_products.html', {scope: $scope, animation: 'slide-in-up'})
            .then(function (modal) {

                for (var j = 0; j < $scope.product_categories.plain().length; j++) {

                    if ($scope.product_categories[j].id == product_category_id) {

                        $scope.product_category = $scope.product_categories[j];

                        var newProduct = Restangular.restangularizeElement('', {}, 'products');
                        newProduct.product_category_id = $scope.product_category.id;

                        $scope.newProducts.push(newProduct);

                    }

                }

                $scope.modal = modal;
                $scope.modal.show();

            });

        $scope.addProduct = function () {

            var newP = Restangular.restangularizeElement('', {}, 'products');
            newP.product_category_id = $scope.product_category.id;

            $scope.newProducts.push(newP);

        }
    }

    function closeProductsModal (){

        for (var l = 0; l < $scope.newProducts.length; l++){

            if ($scope.newProducts[l].product_category_id == $scope.product_category.id){

                if (typeof($scope.newProducts[l].title) == 'undefined'
                    && typeof($scope.newProducts[l].provider) == 'undefined'
                    && typeof($scope.newProducts[l].description) == 'undefined'
                    && typeof($scope.newProducts[l].refund) == 'undefined'){

                    $scope.newProducts.splice(l, 1);

                }

            }

        }

        $scope.modal.hide();

    }


    function openPlanPopover ($event, category) {

        $scope.plan_category = category;

        _.forEach($scope.apartment.plans, function (plan) {

            if (plan.plan_category_id == category.id)
                $scope.plan_category.plan = plan;

        });

        $scope.popover.show($event);

    }

    // function openPlanPopup(mode, category_id) {
    //
    //     if ($scope.popover)
    //         $scope.popover.hide();
    //
    //     $ionicPopup.show({
    //         title: 'בחר/י מקור התמונה',
    //         scope: $scope,
    //         cssClass: 'custom-popup',
    //         buttons: [
    //             {text: 'מצלמה', type: 'button-positive', onTap: function () {addImage(1, mode, category_id);}},
    //             {text: 'גלרייה', type: 'button-calm', onTap: function () {addImage(0, mode, category_id);}},
    //             {text: 'ביטול', type: 'button-assertive', onTap: function () {}}
    //         ]
    //     });
    //
    // }
    //
    // function addImage(index, mode, category_id) {
    //
    //     var options = {
    //         quality: 75,
    //         destinationType: Camera.DestinationType.FILE_URI,
    //         sourceType: index == 1 ? Camera.PictureSourceType.CAMERA : Camera.PictureSourceType.PHOTOLIBRARY,
    //         allowEdit: false,
    //         encodingType: Camera.EncodingType.JPEG,
    //         targetWidth: 600,
    //         targetHeight: 300,
    //         popoverOptions: CameraPopoverOptions,
    //         saveToPhotoAlbum: false,
    //         correctOrientation: true
    //     };
    //
    //     $cordovaCamera.getPicture(options).then(function (fileUri) {
    //
    //         if (mode == 'edit'){
    //
    //             Restangular.one('apartments', $scope.apartment.id).one('plans', $scope.plan_category.plan.id).all('break').post()
    //
    //                 .then(addNewPlan(fileUri, category_id))
    //
    //         }
    //
    //         else
    //             addNewPlan(fileUri, category_id);
    //
    //     });
    // }

    function uploadFile(mode, category_id, file) {

        if ($scope.popover)
            $scope.popover.hide();

        if (mode === 'edit'){

            Restangular.one('apartments', $scope.apartment.id).one('plans', $scope.plan_category.plan.id).all('break').post()

                .then(addNewPlan(file, category_id))

        }

        else
            addNewPlan(file, category_id);

    }

    function addNewPlan(file, category_id) {

        var formData = new FormData();
        formData.append(Utils.randomString(), file);

        Restangular.one('plan_categories', category_id).all('plans').withHttpConfig({transformRequest: angular.identity}).upload(formData)

            .then(function(data) {

                Restangular.one('apartments', $scope.apartment.id).one('plans', data[0].id).all('relationship').post()

                    .then(function() {

                        Restangular.one('apartments', $scope.apartment.id).get().then(function (data) {

                            $ionicPopup.alert({title: "Successfully uploaded!"});
                            $scope.apartment = data;

                        })

                    })

            });

    }

    function deletePlan() {

        Restangular.one('apartments', $scope.apartment.id).one('plans', $scope.plan_category.plan.id).all('break').post().then(function(){

            Restangular.one('plans', $scope.plan_category.plan.id).remove().then(function(){

                Restangular.one('apartments', $scope.apartment.id).get().then(function(data){

                    $scope.apartment = data;
                    $scope.popover.hide();

                })

            })

        });

    }

});
