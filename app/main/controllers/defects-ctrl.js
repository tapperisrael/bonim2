'use strict';
angular.module('main')
.controller('DefectsCtrl', function ($state, $ionicPopup, $scope, $log, $filter, apartment, Restangular) {

    $scope.apartment = apartment;
    $scope.tab = 0;
    $scope.propertyName = 'formatted_created_at';
    $scope.reverse = false;

    $scope.sortBy = sortBy;
    $scope.updateStatus = updateStatus;

    function sortBy (propertyName) {

        $scope.reverse = (propertyName !== null && $scope.propertyName === propertyName) ? !$scope.reverse : true;
        $scope.propertyName = propertyName;
        $scope.apartment.defects = $filter('orderBy')($scope.apartment.defects, $scope.propertyName, $scope.reverse);

    }

    function updateStatus (defect, status) {

        defect = Restangular.restangularizeElement('', defect, 'defects');
        defect.closed = status;
        defect.patch().then(function(data){

            $ionicPopup.alert({title: "Updated!"}).then(function(){

                // $state.go('main.buildings.building.apartments.apartment.defects', {building: apartment.building_id, apartment : apartment.id}, {reload: true});
                $state.reload();

            })

        });

    }

});
