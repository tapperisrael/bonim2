'use strict';
angular.module('main')
.controller('DefectCreateCtrl', function ($localStorage, $ionicPopup, $state, FileService, $log, $scope, Restangular, apartment, building) {

    $scope.apartment = apartment;
    $scope.building = building;
    $scope.newDefect = Restangular.restangularizeElement('', {}, 'defects');

    $scope.makeNewTicket = makeNewTicket;

    function makeNewTicket () {

        var formData = new FormData();
        formData.append('apartment_id', $scope.apartment.id);
        formData.append('user_id', $localStorage.id);
        formData.append('location', $scope.newDefect.location);
        formData.append('description', $scope.newDefect.description);

        if ($scope.newDefect.file){

            FileService.convertFileUriToFile($scope.newDefect.file).then(function(file){

                formData.append('file', file);

                Restangular.one('defect_categories', $scope.newDefect.category).all('defects').withHttpConfig({transformRequest: angular.identity}).upload(formData).then(success)

            })


        } else
            Restangular.one('defect_categories', $scope.newDefect.category).all('defects').withHttpConfig({transformRequest: angular.identity}).upload(formData).then(success)

    }

    function success() {

        Restangular.one('apartments', $scope.apartment.id).get().then(function(){

            $state.go('main.buildings.building.apartments.apartment.defects', {building: apartment.building_id, apartment : apartment.id}, {reload: true});

        })

    }

});
