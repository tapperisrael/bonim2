'use strict';
angular.module('main')
    .controller('MessagesCtrl', function ($scope, apartment, Restangular) {

        $scope.apartment = apartment;
        $scope.message = Restangular.restangularizeElement(Restangular.one('apartments', $scope.apartment.id), {}, 'messages');

        $scope.sendMessage = sendMessage;

        function sendMessage() {

            $scope.message.save().then(function(){

                Restangular.one('apartments', $scope.apartment.id).get().then(function(data){

                    $scope.apartment = data;
                    $scope.message = Restangular.restangularizeElement(Restangular.one('apartments', $scope.apartment.id), {}, 'messages');


                });

            })

        }

    });
