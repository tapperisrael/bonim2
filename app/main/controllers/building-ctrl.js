'use strict';
angular.module('main')
    .controller('BuildingCtrl', function (Restangular, $ionicPopup, $log, $scope, $state, building) {

        // Data

        $scope.building = building;

        // Methods

        $scope.saveBuilding = saveBuilding;
        $scope.addUser = addUser;
        $scope.deleteBuilding = deleteBuilding;
        $scope.deleteConnection = deleteConnection;

        // Functions

        function saveBuilding() {

            $scope.building.save().then(onBuildingSaved, onBuildingSaveError);

        }

        function onBuildingSaved(updatedBuilding) {

            $scope.building = updatedBuilding;

            $state.go('main.projects.project.buildings', {project_id: $scope.building.project_id}, {reload: true});
        }

        function onBuildingSaveError() {

        }

        function deleteBuilding() {

            $scope.building.remove().then(function(){
                $state.go('main.projects.project.buildings', {project_id: $scope.building.project_id}, {reload: true})
            })

        }

        function addUser (x) {

            navigator.contacts.pickContact(function(contact){

                var newUser = Restangular.restangularizeElement('', {}, 'users');

                newUser.phone = contact.phoneNumbers[0].value;
                newUser.phone = newUser.phone.replace(/[^0-9]+/g, '');
                newUser.name = contact.name.formatted ? contact.name.formatted : newUser.phone;
                newUser.email = contact.emails ? contact.emails[0].value : newUser.phone;
                newUser.description = contact.note ? contact.note : newUser.phone;
                newUser.type_role = 'employee';
                newUser.type = 'building';
                newUser.type_id = $scope.building.id;

                Restangular.all('users').customPOST(newUser, 'permissions').then(function(data){

                    $ionicPopup.alert({title: "You have successfully added contact " + data.name});
                    $state.reload();

                });

            },function(err){});

        }

        function deleteConnection(person) {

            var data = {};
            data.user_id = person;
            data.type = 'building';
            data.type_id = $scope.building.id;

            Restangular.all('users').customPOST(data, 'prohibitions').then(function(data){

                $state.reload();

            });

        }


    });
