'use strict';
angular.module('main')
    .controller('GalleryCtrl', function (Upload, $ionicSlideBoxDelegate, $ionicModal, $cordovaCamera, $ionicPopup, $state, FileService, Restangular, $scope, apartment) {

        $scope.apartment = apartment;
        $scope.image = null;
        $scope.picFile = '';

        $ionicModal.fromTemplateUrl('main/templates/modal_image.html', {scope: $scope, animation: 'slide-in-up'}).then(function(modal) {$scope.modal = modal; });

        $scope.addNewImage = addNewImage;
        $scope.closeImageModal = function closeImageModal() {$scope.modal.hide(); $scope.image = null;};
        $scope.deleteImage = deleteImage;

        $scope.goToSlide = function(image) {

            $scope.image = image;
            $scope.modal.show();

        };

        function addNewImage() {

            $ionicPopup.show({
                title: 'בחר/י מקור התמונה',
                scope: $scope,
                cssClass: 'custom-popup',
                buttons: [
                    {text: 'מצלמה', type: 'button-positive', onTap: function () {sendImage(1);}},
                    {text: 'גלרייה', type: 'button-calm', onTap: function () {sendImage(0);}},
                    {text: 'ביטול', type: 'button-assertive', onTap: function () {}}
                ]
            });
            
        }

        function sendImage(index) {

            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: index == 1 ? Camera.PictureSourceType.CAMERA : Camera.PictureSourceType.PHOTOLIBRARY,
                allowEdit: 1,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 600,
                targetHeight: 600,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false,
                correctOrientation: true
            };

            // Get fileUri from gallery (see Camera.DestinationType.FILE_URI in options)
            $cordovaCamera.getPicture(options).then(function (fileUri) {

                FileService.convertFileUriToInternalURL(fileUri).then(function(uri) {

                    FileService.convertFileUriToFile(uri).then(function(file){

                        var formData = new FormData();
                        formData.append('file', file);

                        Restangular.one('apartments', $scope.apartment.id).all('images').withHttpConfig({transformRequest: angular.identity}).upload(formData)
                            .then(function(){

                                Restangular.one('apartments', $scope.apartment.id).get().then(function(data){$scope.apartment = data;})


                            })

                    })

                });

            });

        }

        function deleteImage(image) {

            var confirmPopup = $ionicPopup.confirm({title: 'Are you sure?'});

            confirmPopup.then(function(res) {

                if(res)
                    Restangular.one('images', image.id).remove().then(function (data) {console.log(data);$state.reload();})

            })

        }

    });
