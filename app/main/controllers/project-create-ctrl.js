'use strict';
angular.module('main')
    .controller('ProjectCreateCtrl', function ($ionicPopup, $localStorage, $log, $q, $scope, $state, FileService, Restangular, Utils, project) {

        // Data

        $scope.project = project;
        $scope.localImages = [];
        $scope.autocompleteOptions = {componentRestrictions: {country: 'il'}};

        // Methods

        $scope.saveProject = saveProject;

        // Functions

        function saveProject() {

            // Check for empty fields or any other mistakes

            if (Utils.isAnyFieldEmpty($scope.project.plain()))
                return;


            if (!$scope.project.buildings.length){

                $ionicPopup.alert({title: "Please add at least one building!"});
                return;

            } else {

                for (var i = 0; i < $scope.project.buildings.length; i++){

                    if (typeof $scope.project.buildings[i].apartments_quantity === "undefined" || $scope.project.buildings[i].apartments_quantity === null){

                        $ionicPopup.alert({title: "Every building must have apartments number!"});
                        return;

                    }

                }

            }

            if ($scope.localImages.length == 0){

                $ionicPopup.alert({title: "Please add at least one image!"});
                return;

            }

            if ($scope.project.buildings.length != $scope.project.buildings_quantity){

                $ionicPopup.alert({title: "Please check buildings once again, something is empty!"});
                return;

            }

            if ($scope.project.address.formatted_address)
                $scope.project.address = $scope.project.address.formatted_address;

            for (var j = 0; j < $scope.project.buildings.length; j++){

                if (typeof $scope.project.buildings[j].name === "undefined" || $scope.project.buildings[j].name === '')
                    $scope.project.buildings[j].name = " בניין " + j;

            }

            $scope.project.save().then(onProjectSaved);

        }

        function onProjectSaved(project) {

            FileService.convertFileUrisToFiles($scope.localImages).then(function (files) {

                if (files.length == 0) {

                    $state.go('main.projects', null, {reload: true});
                    return;

                }

                var formData = new FormData();

                files.map(function (file) {

                    formData.append(Utils.randomString(), file);

                });

                formData.append('type', 'project');
                formData.append('id', project.id);

                Restangular.all('files').withHttpConfig({transformRequest: angular.identity}).upload(formData).then(onPicturesSaved);

            });

        }

        function onPicturesSaved() {

            $state.go('main.projects', null, {reload: true});

        }

    });
