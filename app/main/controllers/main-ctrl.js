'use strict';
angular.module('main')
    .controller('MainCtrl', function ($state, $localStorage, $scope) {

        $scope.logout = logout;

        function logout () {

           delete $localStorage.phone;
           delete $localStorage.role;
           delete $localStorage.id;
           $state.go('main.login');

        }

    });
