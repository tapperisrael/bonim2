'use strict';
angular.module('main')
.controller('ConnectionsCtrl', function ($ionicPopup, $state, Restangular, $scope, apartment) {

    $scope.apartment = apartment;

    $scope.deleteConnection = deleteConnection;
    $scope.addUser = addUser;

    function addUser () {

        navigator.contacts.pickContact(function(contact){

            var newUser = Restangular.restangularizeElement('', {}, 'users');

            newUser.phone = contact.phoneNumbers[0].value;
            newUser.phone = newUser.phone.replace(/[^0-9]+/g, '');
            newUser.name = contact.name.formatted ? contact.name.formatted : newUser.phone;
            newUser.email = contact.emails ? contact.emails[0].value : newUser.phone;
            newUser.description = contact.note ? contact.note : newUser.phone;
            newUser.type_role = 'tenant';
            newUser.type = 'apartment';
            newUser.type_id = $scope.apartment.id;

            Restangular.all('users').customPOST(newUser, 'permissions').then(function(data){

                $ionicPopup.alert({title: "You have successfully added contact " + data.name});
                $state.reload();

            });

        },function(err){});

    }


    function deleteConnection(person) {

        var data = {};
        data.user_id = person;
        data.type = 'apartment';
        data.type_id = $scope.apartment.id;

        Restangular.all('users').customPOST(data, 'prohibitions').then(function(data){

            $state.reload();

        });

    }

});
