'use strict';
angular.module('main')
.controller('ProductsCtrl', function ($ionicModal, $log, $scope, apartment) {

    $scope.apartment = apartment;
    $scope.product_category = null;

    $scope.showProductsModal = showProductsModal;
    $scope.closeProductsModal = closeProductsModal;

    function showProductsModal(product_category_id) {

        $ionicModal.fromTemplateUrl('main/templates/modal_show_products.html', {scope: $scope, animation: 'slide-in-up'})
            .then(function (modal) {

                for (var j = 0; j < $scope.apartment.product_categories.length; j++) {

                    if ($scope.apartment.product_categories[j].id == product_category_id)
                        $scope.product_category = $scope.apartment.product_categories[j];

                }

                $scope.modal = modal;
                $scope.modal.show();

            });
    }

    function closeProductsModal (){

        $scope.modal.hide();

    }

});
