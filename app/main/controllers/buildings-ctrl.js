'use strict';
angular.module('main')
    .controller('BuildingsCtrl', function ($log, $scope, project, buildings) {

        // Data

        $scope.buildings = buildings;
        $scope.project = project;

    });
