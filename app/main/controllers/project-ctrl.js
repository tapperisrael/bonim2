'use strict';
angular.module('main')
    .controller('ProjectCtrl', function ($ionicPopup, $localStorage, $log, $q, $scope, $state, FileService, Restangular, Utils, project) {

        // Data

        $scope.project = project;
        $scope.localImages = [];
        $scope.autocompleteOptions = {componentRestrictions: {country: 'il'}};

        // Methods

        $scope.saveProject = saveProject;
        $scope.deleteProject = deleteProject;
        $scope.addUser = addUser;
        $scope.deleteConnection = deleteConnection;
        $scope.deleteImage = deleteImage;

        // Functions

        function saveProject() {

            // Check for empty fields or any other mistakes

            if (Utils.isAnyFieldEmpty($scope.project.plain()))
                return;

            if ($scope.project.address.formatted_address)
                $scope.project.address = $scope.project.address.formatted_address;


            $scope.project.put().then(onProjectSaved);

        }

        function onProjectSaved(project) {

            if ($scope.localImages.length > 0){

                FileService.convertFileUrisToFiles($scope.localImages).then(function (files) {

                    if (files.length == 0) {

                        $state.go('main.projects', null, {reload: true});
                        return;

                    }

                    var formData = new FormData();

                    files.map(function (file) {

                        formData.append(Utils.randomString(), file);

                    });

                    formData.append('type', 'project');
                    formData.append('id', project.id);

                    Restangular.all('files').withHttpConfig({transformRequest: angular.identity}).upload(formData).then(onPicturesSaved);

                });

            } else
                onPicturesSaved();

        }

        function onPicturesSaved() {

            $ionicPopup.alert({title: 'Successfully updated!'});
            $state.go('main.projects', null, {reload: true});

        }

        function deleteProject() {

            $scope.project.remove().then(onProjectDeleted, onProjectDeleteError);

        }

        function onProjectDeleted() {

            $state.go('main.projects', null, {reload: true});

        }

        function onProjectDeleteError() {

        }

        function addUser() {

            navigator.contacts.pickContact(function (contact) {

                var newUser = Restangular.restangularizeElement('', {}, 'users');

                newUser.phone = contact.phoneNumbers[0].value;
                newUser.phone = newUser.phone.replace(/[^0-9]+/g, '');
                newUser.name = contact.name.formatted ? contact.name.formatted : newUser.phone;
                newUser.email = contact.emails ? contact.emails[0].value : newUser.phone;
                newUser.description = contact.note ? contact.note : newUser.phone;
                newUser.type_role = 'employee';
                newUser.type = 'project';
                newUser.type_id = $scope.project.id;

                Restangular.all('users').customPOST(newUser, 'permissions').then(function (data) {

                    $ionicPopup.alert({title: "You have successfully added contact " + data.name});
                    $state.reload();

                });

            }, function (err) {});

        }

        function deleteConnection(person) {

            var data = {};
            data.user_id = person;
            data.type = 'project';
            data.type_id = $scope.project.id;

            Restangular.all('users').customPOST(data, 'prohibitions').then(function () {

                $state.reload();

            });

        }

        function deleteImage(file) {

            var confirmPopup = $ionicPopup.confirm({title: 'Are you sure?'});

            confirmPopup.then(function(res) {

                if(res)
                    Restangular.one('files', file.id).remove().then(function (data) {$state.reload();})

            })

        }

    });