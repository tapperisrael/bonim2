'use strict';
angular.module('main')
    .controller('ProjectsCtrl', function ($localStorage, Restangular, $state, $log, $scope, projects) {

        $scope.projects = projects;

        $scope.moveIntoProject = moveIntoProject;

        function moveIntoProject(proj) {

            if ($localStorage.role != 'tenant'){

                if (proj.buildings_quantity == 1){

                    Restangular.one('users', $localStorage.id).one('projects', proj.id).all('buildings').getList()

                        .then(function(data){

                            $state.go("main.buildings.building.apartments", {building_id : data[0].id});

                        });

                }

                else
                    $state.go("main.projects.project.buildings", {project_id : proj.id});

            }

        }

    });
