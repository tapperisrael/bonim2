'use strict';
angular.module('main')
    .controller('ApartmentsCtrl', function ($ionicPopup, $state, Restangular, $log, $scope, apartments, building, project) {

        // Data

        $scope.apartments = apartments;
        $scope.building = building;
        $scope.project = project;

        // Methods
        $scope.deleteApartment = deleteApartment;

        // Functions

        function deleteApartment(id) {

            var confirmPopup = $ionicPopup.confirm({title: 'Are you sure?'});

            confirmPopup.then(function(res) {

                if(res)
                    Restangular.one('apartments', id).remove().then(function (data) {$state.reload();})

            })
        }
    });
