'use strict';
angular.module('main')
    .service('Utils', function ($ionicPopup) {

        // Public

        this.randomString = randomString;
        this.isAnyFieldEmpty = isAnyFieldEmpty;

        // Private

        function randomString() {

            return (Math.random() * 1e32).toString(36);

        }

        function isAnyFieldEmpty(x){

            var keys = Object.getOwnPropertyNames(x);

            for (var key in keys){

                if (x[keys[key]] === '' || typeof x[keys[key]] === 'undefined' || x[keys[key]] === null){

                    $ionicPopup.alert({title: "נא למלא את כל השדות"});

                    return true;

                }

            }

            return false;

        }

    });
