'use strict';
angular.module('main', [
    'ionic',
    'ngCordova',
    'ui.router',
    'ngStorage',
    'restangular',
    'mgo-angular-wizard',
    'google.places',
    'ionic.cloud',
    'ngFileUpload'
])
    .config(function ($ionicCloudProvider, $compileProvider, $stateProvider, $urlRouterProvider) {

        $ionicCloudProvider.init({
            'core': {
                'app_id': 'a4fcfa2e'
            }
        });

        // Needed to be able to display images in LiveReload mode on devices
        $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|file|blob|cdvfile):|data:image\//);

        // ROUTING with ui.router
        $urlRouterProvider.otherwise('/main/login');

        $stateProvider
            .state('main', {
                abstract: true,
                controller: 'MainCtrl',
                templateUrl: 'main/templates/main.html',
                url: '/main'
            })
            .state('main.login', {
                cache: false,
                url: '/login',
                views: {
                    'content': {
                        controller: 'LoginCtrl',
                        templateUrl: 'main/templates/login.html'
                    }
                }
            })

            .state('main.tenants', {
                abstract: true,
                url: '/tenants'
            })

            // List apartments for tenant
            .state('main.tenants.tenant', {
                cache: false,
                url: '/:tenant_id',
                views: {
                    'content@main': {
                        controller: 'TenantCtrl',
                        templateUrl: 'main/templates/tenant.html'
                    }
                },
                resolve: {tenant_apartments: function(Restangular, $stateParams){ return Restangular.one('users', $stateParams.tenant_id).all('apartments').getList()}}
            })

            // Profile
            .state('main.user', {
                cache: false,
                url: '/user',
                views: {
                    'content@main': {
                        controller: 'UserCtrl',
                        templateUrl: 'main/templates/user.html'
                    }
                },
                resolve: {user: function(Restangular, $localStorage){ return Restangular.one('users', $localStorage.id).get()}}
            })

            // List All Projects
            .state('main.projects', {
                cache: false,
                resolve: {projects: function (Restangular, $localStorage) {return Restangular.one('users', $localStorage.id).all('projects').getList();}},
                url: '/projects',
                views: {
                    'content': {
                        controller: 'ProjectsCtrl',
                        templateUrl: 'main/templates/projects.html'
                    }
                }
            })

            .state('main.projects_list', {
                cache: false,
                resolve: {projects: function (Restangular) {return Restangular.one('users', '1').all('projects').getList();}},
                url: '/projects_list',
                views: {
                    'content': {
                        controller: 'ProjectsCtrl',
                        templateUrl: 'main/templates/projects.html'
                    }
                }
            })

            // Create New Projects
            .state('main.projects.create', {
                cache: false,
                resolve: {project: function (Restangular) {return Restangular.restangularizeElement(null, {name: '', address: '', buildings_quantity: '', status: '', buildings : []}, 'projects');}},
                url: '/create',
                views: {
                    'content@main': {
                        controller: 'ProjectCreateCtrl',
                        templateUrl: 'main/templates/project_create.html'
                    }
                }
            })

            // Edit Existing Project
            .state('main.projects.project', {
                cache: false,
                resolve: {project: function ($stateParams, Restangular) {return Restangular.one('projects', $stateParams.project_id).get();}},
                url: '/:project_id',
                views: {
                    'content@main': {
                        controller: 'ProjectCtrl',
                        templateUrl: 'main/templates/project.html'
                    }
                }
            })

            // List All Buildings Belonging to Project
            .state('main.projects.project.buildings', {
                cache: false,
                resolve: {buildings: function ($stateParams, Restangular, $localStorage) {return Restangular.one('users', $localStorage.id).one('projects', $stateParams.project_id).all('buildings').getList();}},
                url: '/buildings',
                views: {
                    'content@main': {
                        controller: 'BuildingsCtrl',
                        templateUrl: 'main/templates/buildings.html'
                    }
                }
            })

            // Create New Buildings Belonging To Project
            .state('main.projects.project.buildings.create', {
                cache: false,
                resolve: {building: function ($stateParams, Restangular) {return Restangular.restangularizeElement(Restangular.one('projects', $stateParams.project_id), {}, 'buildings');}},
                url: '/create',
                views: {
                    'content@main': {
                        controller: 'BuildingCtrl',
                        templateUrl: 'main/templates/building.html'
                    }
                }
            })

            .state('main.buildings', {
                abstract: true,
                url: '/buildings'
            })


            // Edit Existing Building
            .state('main.buildings.building', {
                cache: false,
                resolve: {building: function ($stateParams, Restangular) {return Restangular.one('buildings', $stateParams.building_id).get();}},
                url: '/:building_id',
                views: {
                    'content@main': {
                        controller: 'BuildingCtrl',
                        templateUrl: 'main/templates/building.html'
                    }
                }
            })

            // List All Apartments Belonging To Building
            .state('main.buildings.building.apartments', {
                cache: false,
                resolve: {
                    apartments: function ($stateParams, $localStorage, Restangular) {
                        return Restangular.one('users', $localStorage.id).one('buildings', $stateParams.building_id).all('apartments').getList();
                    },
                    product_categories: function ($stateParams, building, Restangular) {
                        return Restangular.one('projects', building.project_id).all('product_categories').getList();
                    },
                    plan_categories: function ($stateParams, building, Restangular) {
                        return Restangular.one('projects', building.project_id).all('plan_categories').getList();
                    },
                    project: function ($stateParams, building, Restangular) {
                        return Restangular.one('projects', building.project_id).get();
                    }
                },
                url: '/apartments',
                views: {
                    'content@main': {
                        controller: 'ApartmentsCtrl',
                        templateUrl: 'main/templates/apartments.html'
                    }
                }
            })

            // Create New Apartment Belonging To Building
            .state('main.buildings.building.apartments.create', {
                cache: false,
                url: '/create',
                views: {
                    'content@main': {
                        controller: 'ApartmentCreateCtrl',
                        templateUrl: 'main/templates/apartment_create.html'
                    }
                }
            })

            // Look into the apartment
            .state('main.buildings.building.apartments.apartment', {
                cache: false,
                resolve: {apartment: function ($stateParams, Restangular) {return Restangular.one('apartments', $stateParams.apartment_id).get();}},
                url: '/:apartment_id',
                views: {
                    'content@main': {
                        controller: 'ApartmentCtrl',
                        templateUrl: 'main/templates/apartment.html'
                    }
                }
            })

            // Look into the apartment
            .state('main.buildings.building.apartments.apartment_edit', {
                cache: false,
                resolve: {apartment: function ($stateParams, Restangular) {return Restangular.one('apartments', $stateParams.apartment_id).get();}},
                url: '/:apartment_id/edit',
                views: {
                    'content@main': {
                        controller: 'ApartmentEditCtrl',
                        templateUrl: 'main/templates/apartment_edit.html'
                    }
                }
            })

            // Look into the apartment products
            .state('main.buildings.building.apartments.apartment.products', {
                cache: false,
                url: '/products',
                views: {
                    'content@main': {
                        controller: 'ProductsCtrl',
                        templateUrl: 'main/templates/products.html'
                    }
                }
            })

            // Look into the apartment plans
            .state('main.buildings.building.apartments.apartment.plans', {
                cache: false,
                url: '/plans',
                views: {
                    'content@main': {
                        controller: 'PlansCtrl',
                        templateUrl: 'main/templates/plans.html'
                    }
                }
            })

            // Look into the apartment defects
            .state('main.buildings.building.apartments.apartment.defects', {
                cache: false,
                url: '/defects',
                views: {
                    'content@main': {
                        controller: 'DefectsCtrl',
                        templateUrl: 'main/templates/defects.html'
                    }
                }
            })


            // Create new defect
            .state('main.buildings.building.apartments.apartment.defects.create', {
                cache: false,
                url: '/defect_create',
                views: {
                    'content@main': {
                        controller: 'DefectCreateCtrl',
                        templateUrl: 'main/templates/defect_create.html'
                    }
                }
            })


            // Look into the apartment defect
            .state('main.buildings.building.apartments.apartment.defects.defect', {
                cache: false,
                url: '/:defect_id',
                views: {
                    'content@main': {
                        controller: 'DefectCtrl',
                        templateUrl: 'main/templates/defect.html'
                    }
                },
                resolve: {defect : function($stateParams, Restangular){return Restangular.one('defects', $stateParams.defect_id).get()}}
            })

            .state('main.buildings.building.apartments.apartment.connections', {
                cache: false,
                url: '/connections',
                views: {
                    'content@main': {
                        controller: 'ConnectionsCtrl',
                        templateUrl: 'main/templates/connections.html'
                    }
                }
            })

            // Look into the apartment gallery
            .state('main.buildings.building.apartments.apartment.gallery', {
                cache: false,
                url: '/gallery',
                views: {
                    'content@main': {
                        controller: 'GalleryCtrl',
                        templateUrl: 'main/templates/gallery.html'
                    }
                }
            })

            // Look into the apartment chat
            .state('main.buildings.building.apartments.apartment.messages', {
                cache: false,
                url: '/messages',
                views: {
                    'content@main': {
                        controller: 'MessagesCtrl',
                        templateUrl: 'main/templates/messages.html'
                    }
                }
            })

            ;

    })

    .run(function ($ionicPopup, $ionicLoading, $ionicHistory, $localStorage, $log, $rootScope, $state, Config, Restangular) {

        $rootScope.options = {loop: true, effect: 'slide', autoplay: false};
        $rootScope.enableBack = false;
        $rootScope.enableMenu = false;
        $rootScope.role = $localStorage.role;
        $rootScope.goBack = function(){$ionicHistory.goBack();};

        $rootScope.redirectUser = function () {

            if ($localStorage.role != 'tenant'){

                $state.go('main.projects');

            } else {
                console.log('here');
                // return;
                Restangular.one('users', $localStorage.id).all('apartments').getList()
                    .then(function(response){

                        if (response.length == 0){

                            $ionicPopup.alert({title: 'There is no apartments for this user!'});
                            delete $localStorage.id;
                            $state.go('main.login');

                        }

                        if (response.length == 1)
                            $state.go('main.buildings.building.apartments.apartment', {building_id: response[0].apartment.building_id, apartment_id: response[0].apartment.id}, {reload: true});
                        else
                            $state.go('main.tenants.tenant', {tenant_id : $localStorage.id}, {reload: true});

                    });

            }
        };

        $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {

            if($localStorage.role != 'tenant')
                $state.go('main.projects');
            else
                console.log('error!!!');

        });

        $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams, options) {

            if (toState.name === 'main.projects' ||
                toState.name === 'main.tenants.tenant' ||
                toState.name === 'main.user' ||
                toState.name === 'main.buildings.building.apartments.create'){

                $rootScope.enableBack = false;
                $rootScope.enableMenu = true;

            } else {

                $rootScope.enableBack = true;
                $rootScope.enableMenu = false;

            }

        });

        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {

            if (toState.name === "main.login" && $localStorage.id) {

                event.preventDefault();
                $rootScope.redirectUser();

            }

        });

        // See app/main/constants/env-dev.json and app/main/constants/env-prod.json
        Restangular.setBaseUrl(Config.ENV.SERVER_URL);

        // Attach phone number as token for each API request
        Restangular.addFullRequestInterceptor(function (element, operation, route, url, headers) {

            return {
                headers: _.merge({Authorization: 'Bearer ' + $localStorage.phone}, headers)
            };

        });

        Restangular.addRequestInterceptor(function(element, operation, what, url){

            $ionicLoading.show({

                template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'

            });

            return element;

        });

        // Handle response
        Restangular.addResponseInterceptor(function (data, operation, what, url) {

            if (data.data) {
                $log.log(url, Restangular.copy(data.data).plain());
            }

            var extractedData;

            if (operation === 'getList') {

                extractedData = data.data;

            } else {

                extractedData = data.data;

            }

            $ionicLoading.hide();

            return extractedData;

        });

        Restangular.setErrorInterceptor(function(response, deferred, responseHandler) {

            if (response){

                $ionicLoading.hide();

                switch (response.data.code){

                    case 401:
                        $ionicPopup.alert({title: "מספר טלפון לא נכון"});
                        break;

                    case 404:
                        $ionicPopup.alert({title: 'There is no apartments for this user!'});
                        delete $localStorage.id;
                        $state.go('main.login');
                        break;

                    default:
                        $ionicPopup.alert({title: 'Error happened!'});
                        break;

                }

            }

            return response;
        });

        // Add custom method upload for images so it will be possible to call

        Restangular.addElementTransformer('images', true, function (image) {

            image.addRestangularMethod('upload', 'post', undefined, undefined, {'Content-Type': undefined});

            return image;
        });

        Restangular.addElementTransformer('files', true, function (image) {

            image.addRestangularMethod('upload', 'post', undefined, undefined, {'Content-Type': undefined});

            return image;
        });

        Restangular.addElementTransformer('products', true, function (image) {

            image.addRestangularMethod('upload', 'post', undefined, undefined, {'Content-Type': undefined});

            return image;
        });

        Restangular.addElementTransformer('plans', true, function (image) {

            image.addRestangularMethod('upload', 'post', undefined, undefined, {'Content-Type': undefined});

            return image;
        });

        Restangular.addElementTransformer('defects', true, function (image) {

            image.addRestangularMethod('upload', 'post', undefined, undefined, {'Content-Type': undefined});

            return image;
        });

        Restangular.addElementTransformer('comments', true, function (image) {

            image.addRestangularMethod('upload', 'post', undefined, undefined, {'Content-Type': undefined});

            return image;
        });

        Restangular.addElementTransformer('images', true, function (image) {

            image.addRestangularMethod('upload', 'post', undefined, undefined, {'Content-Type': undefined});

            return image;
        });


        Restangular.addElementTransformer('avatar', true, function (image) {

            image.addRestangularMethod('upload', 'post', undefined, undefined, {'Content-Type': undefined});

            return image;
        });
    });
