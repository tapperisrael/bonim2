'use strict';

describe('module: main, controller: Defect_createCtrl', function () {

  // load the controller's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var Defect_createCtrl;
  beforeEach(inject(function ($controller) {
    Defect_createCtrl = $controller('Defect_createCtrl');
  }));

  it('should do something', function () {
    expect(!!Defect_createCtrl).toBe(true);
  });

});
