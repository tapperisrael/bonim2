'use strict';

describe('module: main, controller: ApartmentEditCtrl', function () {

  // load the controller's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var Apartment_editCtrl;
  beforeEach(inject(function ($controller) {
    Apartment_editCtrl = $controller('Apartment_editCtrl');
  }));

  it('should do something', function () {
    expect(!!Apartment_editCtrl).toBe(true);
  });

});
