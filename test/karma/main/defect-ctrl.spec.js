'use strict';

describe('module: main, controller: DefectCtrl', function () {

  // load the controller's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var DefectCtrl;
  beforeEach(inject(function ($controller) {
    DefectCtrl = $controller('DefectCtrl');
  }));

  it('should do something', function () {
    expect(!!DefectCtrl).toBe(true);
  });

});
