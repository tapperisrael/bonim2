'use strict';

describe('module: main, controller: ConnectionsCtrl', function () {

  // load the controller's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var ConnectionsCtrl;
  beforeEach(inject(function ($controller) {
    ConnectionsCtrl = $controller('ConnectionsCtrl');
  }));

  it('should do something', function () {
    expect(!!ConnectionsCtrl).toBe(true);
  });

});
