'use strict';

describe('module: main, controller: ProjectsCtrl', function () {

  // load the controller's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var ProjectsCtrl;
  beforeEach(inject(function ($controller) {
    ProjectsCtrl = $controller('ProjectsCtrl');
  }));

  it('should do something', function () {
    expect(!!ProjectsCtrl).toBe(true);
  });

});
