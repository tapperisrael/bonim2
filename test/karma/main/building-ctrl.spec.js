'use strict';

describe('module: main, controller: BuildingCtrl', function () {

  // load the controller's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var BuildingCtrl;
  beforeEach(inject(function ($controller) {
    BuildingCtrl = $controller('BuildingCtrl');
  }));

  it('should do something', function () {
    expect(!!BuildingCtrl).toBe(true);
  });

});
