'use strict';

describe('module: main, controller: BuildingsCtrl', function () {

  // load the controller's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var BuildingsCtrl;
  beforeEach(inject(function ($controller) {
    BuildingsCtrl = $controller('BuildingsCtrl');
  }));

  it('should do something', function () {
    expect(!!BuildingsCtrl).toBe(true);
  });

});
