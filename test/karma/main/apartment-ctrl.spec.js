'use strict';

describe('module: main, controller: ApartmentCtrl', function () {

  // load the controller's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var ApartmentCtrl;
  beforeEach(inject(function ($controller) {
    ApartmentCtrl = $controller('ApartmentCtrl');
  }));

  it('should do something', function () {
    expect(!!ApartmentCtrl).toBe(true);
  });

});
