'use strict';

describe('module: main, controller: DefectsCtrl', function () {

  // load the controller's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var DefectsCtrl;
  beforeEach(inject(function ($controller) {
    DefectsCtrl = $controller('DefectsCtrl');
  }));

  it('should do something', function () {
    expect(!!DefectsCtrl).toBe(true);
  });

});
